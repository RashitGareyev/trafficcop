package ru.inovus.exception;

public class FileIsEmtyException extends RuntimeException {
    public FileIsEmtyException() {
    }

    public FileIsEmtyException(String message) {
        super(message);
    }

    public FileIsEmtyException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileIsEmtyException(Throwable cause) {
        super(cause);
    }

    public FileIsEmtyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
