package ru.inovus.exception;

public class IllegalStatusException extends RuntimeException {
    public IllegalStatusException() {
    }

    public IllegalStatusException(String message) {
        super(message);
    }

    public IllegalStatusException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalStatusException(Throwable cause) {
        super(cause);
    }

    public IllegalStatusException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
