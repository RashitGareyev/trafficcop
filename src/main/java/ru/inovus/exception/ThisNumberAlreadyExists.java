package ru.inovus.exception;

public class ThisNumberAlreadyExists extends RuntimeException {
    public ThisNumberAlreadyExists() {
    }

    public ThisNumberAlreadyExists(String message) {
        super(message);
    }

    public ThisNumberAlreadyExists(String message, Throwable cause) {
        super(message, cause);
    }

    public ThisNumberAlreadyExists(Throwable cause) {
        super(cause);
    }

    public ThisNumberAlreadyExists(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
