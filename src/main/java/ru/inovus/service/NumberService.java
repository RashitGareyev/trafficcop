package ru.inovus.service;

import org.springframework.stereotype.Service;
import ru.inovus.entity.NumberEntity;
import ru.inovus.exception.FileIsEmtyException;
import ru.inovus.exception.IllegalStatusException;
import ru.inovus.exception.ThisNumberAlreadyExists;
import ru.inovus.repository.NumberRepository;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class NumberService {
    private final NumberRepository numberRepository;
    private static final char[] LETTERS = {'A', 'B', 'C', 'E', 'H', 'K', 'M', 'O', 'P', 'T', 'X', 'Y'};
    private static final String REGION = " 116 RUS";
    private static final int MAX_DIGIT = 9;


    public NumberService(NumberRepository numberRepository) {
        this.numberRepository = numberRepository;
    }


    public String randomNumber() {
        String number = "" + randomLetter() + threeDigits() + randomLetter() + randomLetter() + REGION;
        return saveNumber(number);
    }

    public String nextNumber() {
        String lastNumber = readLastNumber();
        int pureNumber = pureNumber(lastNumber);
        String nextNumber = "" + nextFirstLetter(lastNumber) + nextPureNumber(pureNumber) + nextSecondLetter(lastNumber) + nextThirdLetter(lastNumber) + REGION;
        return saveNumber(nextNumber);
    }


    private char randomLetter() {
        return LETTERS[(int) (Math.random() * (LETTERS.length - 1))];
    }

    private String threeDigits() {
        String threeDigit = "" + (int) (Math.random() * MAX_DIGIT) + (int) (Math.random() * MAX_DIGIT) + (int) (Math.random() * MAX_DIGIT);
        return threeDigit;
    }


    private void saveLastNumber(String lastNumber) {
        try {
            FileWriter writer = new FileWriter("LastNumber.txt");
            writer.write(lastNumber);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String readLastNumber() {
        File lastNumberFile = new File("LastNumber.txt");
        String lastNumber = null;
        try {
            FileReader fileReader = new FileReader(lastNumberFile);
            BufferedReader reader = new BufferedReader(fileReader);

            if ((lastNumber = reader.readLine()) == null) {
                throw new FileIsEmtyException();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastNumber;
    }


    private String saveNumber(String number) {
        if (numberRepository.findByNumber(number).size() == 0) {
            NumberEntity numberEntity = new NumberEntity();
            numberEntity.setNumber(number);
            numberRepository.save(numberEntity);
            saveLastNumber(number);
            return number;
        } else {
            throw new ThisNumberAlreadyExists();
        }
    }


    private int pureNumber(@NotNull String lastNumber) {
        char[] numberChars = lastNumber.toCharArray();
        int firstNumber = Integer.parseInt(String.valueOf(numberChars[1]));
        int secondNumber = Integer.parseInt(String.valueOf(numberChars[2]));
        int thirdNumber = Integer.parseInt(String.valueOf(numberChars[3]));
        int pureNumber = 100 * firstNumber + 10 * secondNumber + thirdNumber;
        return pureNumber;
    }

    public int getPureNumber(String lastNumber) {
        return pureNumber(lastNumber);
    }

    @NotNull
    private String nextPureNumber(int pureNumber) {

        int tempPureNumber = 1000 + pureNumber + 1;
        char[] chars = ("" + tempPureNumber).toCharArray();
        return "" + chars[1] + chars[2] + chars[3];
    }

    public String getNextPureNumber(int pureNumber) {
        return nextPureNumber(pureNumber);
    }


    private char nextLetter(char letter) {
        List<Character> letters = new ArrayList<>(List.of('A', 'B', 'C', 'E', 'H', 'K', 'M', 'O', 'P', 'T', 'X', 'Y'));
        char result;
        if (letter == 'Y') {
            result = 'A';
        } else {
            result = letters.get(letters.indexOf(letter) + 1);
        }
        return result;
    }

    public char getNextLetter(char letter) {
        return nextLetter(letter);
    }

    private char nextThirdLetter(String lastNumber) {
        int pureNumber = pureNumber(lastNumber);
        char[] numberChars = lastNumber.toCharArray();
        char firstLetter = numberChars[0];
        char secondLetter = numberChars[4];
        char thirdLetter = numberChars[5];
        if (pureNumber == 999 && (firstLetter != 'Y' || secondLetter != 'Y' || thirdLetter != 'Y')) {
            return nextLetter(thirdLetter);
        } else if (pureNumber == 999 && firstLetter == 'Y' && secondLetter == 'Y' && thirdLetter == 'Y') {
            throw new IllegalStatusException();
        } else {
            return thirdLetter;
        }
    }

    public char getNextThirdLetter(String lastNumber) {
        return nextThirdLetter(lastNumber);
    }

    private char nextSecondLetter(@NotNull String lastNumber) {
        char[] numberChars = lastNumber.toCharArray();
        char firstLetter = numberChars[0];
        char secondLetter = numberChars[4];
        char thirdLetter = numberChars[5];
        int pureNumber = pureNumber(lastNumber);


        if (thirdLetter == 'Y' && pureNumber == 999 && (firstLetter != 'Y' || secondLetter != 'Y')) {
            return nextLetter(secondLetter);
        } else if (pureNumber == 999 && firstLetter == 'Y' && secondLetter == 'Y' && thirdLetter == 'Y') {
            throw new IllegalStatusException();
        } else {
            return secondLetter;
        }
    }

    public char getNextSecondLetter(String lastNumber) {
        return nextSecondLetter(lastNumber);
    }

    private char nextFirstLetter(@NotNull String lastNumber) {
        char[] numberChars = lastNumber.toCharArray();
        char firstLetter = numberChars[0];
        char secondLetter = numberChars[4];
        char thirdLetter = numberChars[5];
        int pureNumber = pureNumber(lastNumber);

        if (pureNumber == 999 && firstLetter != 'Y' && secondLetter == 'Y') {
            return nextLetter(firstLetter);
        } else if (pureNumber == 999 && firstLetter == 'Y' && secondLetter == 'Y' && thirdLetter == 'Y') {
            throw new IllegalStatusException();
        } else {
            return firstLetter;
        }
    }

    public char getNextFirstLetter(String lastNumber) {
        return nextFirstLetter(lastNumber);
    }


}
