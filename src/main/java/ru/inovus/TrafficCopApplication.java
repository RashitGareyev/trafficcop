package ru.inovus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrafficCopApplication {

    public static void main(String[] args) {

        SpringApplication.run(TrafficCopApplication.class, args);
    }
}
