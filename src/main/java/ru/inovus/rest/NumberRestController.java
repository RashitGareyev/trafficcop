package ru.inovus.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.inovus.service.NumberService;

@CrossOrigin
@RestController
@RequestMapping("/number")
public class NumberRestController {
    private final NumberService service;

    public NumberRestController(NumberService service) {
        this.service = service;
    }

    @GetMapping("/random")
    public String randomNumber(){
        return service.randomNumber();
    }

    @GetMapping("/next")
    public String nextNumber(){
        return service.nextNumber();
    }



}
