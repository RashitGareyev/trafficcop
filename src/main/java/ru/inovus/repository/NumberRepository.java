package ru.inovus.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.inovus.entity.NumberEntity;

import java.util.List;

public interface NumberRepository extends JpaRepository<NumberEntity, Integer> {

    @Query("SELECT e FROM NumberEntity e WHERE e.number = :number")
    List<NumberEntity> findByNumber (@Param("number") String number);

}
