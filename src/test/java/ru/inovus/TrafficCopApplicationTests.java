package ru.inovus;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.inovus.exception.IllegalStatusException;
import ru.inovus.repository.NumberRepository;
import ru.inovus.service.NumberService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TrafficCopApplicationTests {
    private NumberService service;
    private NumberRepository numberRepository;


    @Test
    public void contextLoads() {
    }

    @Test
    @DisplayName("Single next letter (boundary)")
    public void testNextLetter() {
        service = new NumberService(numberRepository);
        char letter = service.getNextLetter('Y');
        Assertions.assertEquals('A', letter, "Letter must be equal A");
    }

    @Test
    @DisplayName("Single next letter (neighboring)")
    public void testNextLetterNeighboring() {
        service = new NumberService(numberRepository);
        char letter = service.getNextLetter('H');
        Assertions.assertEquals('K', letter, "Letter must be equal K");
    }

    @Test
    @DisplayName("Integer pure number (zero natural number)")
    public void testPureNumberZeroNaturalNumber() {
        service = new NumberService(numberRepository);
        int pureNumber = service.getPureNumber("A000AA 116RUS");
        Assertions.assertEquals(0, pureNumber, "Pure number must be equal zero");
    }

    @Test
    @DisplayName("Integer pure number (one natural number)")
    public void testPureNumberOneNaturalNumber() {
        service = new NumberService(numberRepository);
        int pureNumber = service.getPureNumber("A010AA 116RUS");
        Assertions.assertEquals(10, pureNumber, "Pure number must be equal ten");
    }

    @Test
    @DisplayName("Integer pure number (Three natural number)")
    public void testPureNumberThreeNaturalNumber() {
        service = new NumberService(numberRepository);
        int pureNumber = service.getPureNumber("A111AA 116RUS");
        Assertions.assertEquals(111, pureNumber, "Pure number must be equal 111");
    }


    @Test
    @DisplayName("String next pure number (zero natural number)")
    public void testNextPureNumberZeroNaturalNumber() {
        service = new NumberService(numberRepository);
        String nextPureNumber = service.getNextPureNumber(0);
        Assertions.assertEquals("001", nextPureNumber, "Next pure number must be equals 001");
    }


    @Test
    @DisplayName("String next pure number (one natural number)")
    public void testNextPureNumberOneNaturalNumber() {
        service = new NumberService(numberRepository);
        String nextPureNumber = service.getNextPureNumber(10);
        Assertions.assertEquals("011", nextPureNumber, "Next pure number must be equals 011");
    }

    @Test
    @DisplayName("String next pure number (three natural number)")
    public void testNextPureNumberThreeNaturalNumber() {
        service = new NumberService(numberRepository);
        String nextPureNumber = service.getNextPureNumber(111);
        Assertions.assertEquals("112", nextPureNumber, "Next pure number must be equals 112");
    }

    @Test
    @DisplayName("String next pure number (boundary)")
    public void testNextPureNumberBoundary() {
        service = new NumberService(numberRepository);
        String nextPureNumber = service.getNextPureNumber(999);
        Assertions.assertEquals("000", nextPureNumber, "Next pure number must be equals 000");
    }

    //    1.1
    @Test
    @DisplayName("Next third letter (pure number not boundary, third letter not boundary)")
    public void testNextThirdLetterPureNumberNotBoundaryThirdLetterNotBoundary() {
        service = new NumberService(numberRepository);
        char nextThirdLetter = service.getNextThirdLetter("A555AB 116RUS");
        Assertions.assertEquals('B', nextThirdLetter, "Next third letter must be equals B");
    }

    //1.2
    @Test
    @DisplayName("Next third letter (pure number not boundary, third letter boundary)")
    public void testNextThirdLetterPureNumberNotBoundaryThirdLetterBoundary() {
        service = new NumberService(numberRepository);
        char nextThirdLetter = service.getNextThirdLetter("A555AY 116RUS");
        Assertions.assertEquals('Y', nextThirdLetter, "Next third letter must be equals Y");
    }

    //1.3
    @Test
    @DisplayName("Next third letter (pure number boundary, third letter not boundary)")
    public void testNextThirdLetterPureNumberBoundaryThirdLetterNotBoundary() {
        service = new NumberService(numberRepository);
        char nextThirdLetter = service.getNextThirdLetter("Y999YB 116RUS");
        Assertions.assertEquals('C', nextThirdLetter, "Next third letter must be equals C");
    }

    //1.4
    @Test
    @DisplayName("Next third letter (pure number boundary, only third letter boundary)")
    public void testNextThirdLetterPureNumberBoundaryOnlyThirdLetterBoundary() {
        service = new NumberService(numberRepository);
        char nextThirdLetter = service.getNextThirdLetter("A999AY 116RUS");
        Assertions.assertEquals('A', nextThirdLetter, "Next third letter must be equals A");
    }

    //1.5
    @Test
    @DisplayName("Next third letter (pure number not boundary, all letter boundary)")
    public void testNextThirdLetterPureNumberBoundaryAllLetterBoundary() {
        service = new NumberService(numberRepository);
        char nextThirdLetter = service.getNextThirdLetter("Y555YY 116RUS");
        Assertions.assertEquals('Y', nextThirdLetter, "Next third letter must be equals Y");
    }

    //1.6
    @Test
    @DisplayName("Next third letter (pure number boundary, all letters boundary)")
    public void testNextThirdLetterPureNumberBoundaryAllLettersBoundary() throws IllegalStatusException {
        service = new NumberService(numberRepository);
        try {
            char nextThirdLetter = service.getNextThirdLetter("Y999YY 116RUS");
            Assert.fail("Expected IllegalStatusException");
        } catch (IllegalStatusException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }

    //2.1
    @Test
    @DisplayName("Next second letter (pure number not boundary, second letter nor boundary)")
    public void testNextSecondLetterPureNumberNotBoundarySecondLetterNotBoundary() {
        service = new NumberService(numberRepository);
        char nextSecondLetter = service.getNextSecondLetter("A555AB 116RUS");
        Assertions.assertEquals('A', nextSecondLetter, "Next second letter must be equals A");
    }

    //2.2
    @Test
    @DisplayName("Next second letter (pure number not boundary, second and third letter boundary)")
    public void testNextSecondLetterPureNumberNotBoundarySecondAndThirdLetterBoundary() {
        service = new NumberService(numberRepository);
        char nextSecondLetter = service.getNextSecondLetter("A555YY 116RUS");
        Assertions.assertEquals('Y', nextSecondLetter, "Next second letter must be equals Y");
    }


    //2.3
    @Test
    @DisplayName("Next second letter (pure number boundary, third letter not boundary)")
    public void testNextSecondLetterPureNumberBoundaryThirdLetterNotBoundary() {
        service = new NumberService(numberRepository);
        char nextSecondLetter = service.getNextSecondLetter("A999AB 116RUS");
        Assertions.assertEquals('A', nextSecondLetter, "Next second letter must be equals A");
    }

    //2.4
    @Test
    @DisplayName("Next second letter (pure number boundary, only third letter boundary)")
    public void testNextSecondLetterPureNumberBoundaryOnlyThirdLetterBoundary() {
        service = new NumberService(numberRepository);
        char nextSecondLetter = service.getNextSecondLetter("A999AY 116RUS");
        Assertions.assertEquals('B', nextSecondLetter, "Next second letter must be equals B");
    }

    //2.5
    @Test
    @DisplayName("Next second letter (pure number boundary, second and third letters boundary)")
    public void testNextSecondLetterPureNumberBoundarySecondAndThirdLettersBoundary() {
        service = new NumberService(numberRepository);
        char nextSecondLetter = service.getNextSecondLetter("A999YY 116RUS");
        Assertions.assertEquals('A', nextSecondLetter, "Next second letter must be equals A");
    }

    //2.6
    @Test
    @DisplayName("Next second letter (pure number not boundary, all letters boundary)")
    public void testNextSecondLetterPureNumberNotBoundaryAllLettersBoundary() {
        service = new NumberService(numberRepository);
        char nextSecondLetter = service.getNextSecondLetter("Y555YY 116RUS");
        Assertions.assertEquals('Y', nextSecondLetter, "Next second letter must be equals Y");
    }


    //2.7
    @Test
    @DisplayName("Next second letter (pure number boundary, all letters boundary)")
    public void testNextSecondLetterPureNumberBoundaryAllLettersBoundary() throws IllegalStatusException {
        service = new NumberService(numberRepository);
        try {
            char nextSecondLetter = service.getNextSecondLetter("Y999YY 116RUS");
            Assert.fail("Expected IllegalStatusException");
        } catch (IllegalStatusException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }

    //3.1
    @Test
    @DisplayName("Next first letter (pure number not boundary, second and third letters not boundary)")
    public void testNextFirstLetterPureNumberNotBoundarySecondAndThirdLettersNotBoundary() {
        service = new NumberService(numberRepository);
        char nextFirstLetter = service.getNextFirstLetter("A555AB 116RUS");
        Assertions.assertEquals('A', nextFirstLetter, "Next first letter must be equals A");
    }

    //3.2
    @Test
    @DisplayName("Next first letter (pure number not boundary, second and third letters boundary)")
    public void testNextFirstLetterPureNumberNotBoundarySecondAndThirdLettersBoundary() {
        service = new NumberService(numberRepository);
        char nextFirstLetter = service.getNextFirstLetter("A555YY 116RUS");
        Assertions.assertEquals('A', nextFirstLetter, "Next first letter must be equals A");
    }

    //3.3
    @Test
    @DisplayName("Next first letter (pure number boundary, only second and third letter boundary)")
    public void testNextFirstLetterPureNumberBoundaryOnlyThirdLetterBoundary() {
        service = new NumberService(numberRepository);
        char nextFirstLetter = service.getNextFirstLetter("A999YY 116RUS");
        Assertions.assertEquals('B', nextFirstLetter, "Next first letter must be equals B");
    }


    //3.4
    @Test
    @DisplayName("Next first letter (pure number not boundary, all letters boundary)")
    public void testNextFirstLetterPureNumberNotBoundaryAllLetterBoundary() {
        service = new NumberService(numberRepository);
        char nextFirstLetter = service.getNextFirstLetter("Y555YY 116RUS");
        Assertions.assertEquals('Y', nextFirstLetter, "Next first letter must be equals Y");
    }

    //3.5
    @Test
    @DisplayName("Next first letter (pure number boundary, all letters boundary)")
    public void testNextFirstLetterPureNumberBoundaryAllLettersBoundary() throws IllegalStatusException {
        service = new NumberService(numberRepository);
        try {
            char nextFirstLetter = service.getNextFirstLetter("Y999YY 116RUS");
            Assert.fail("Expected IllegalStatusException");
        } catch (IllegalStatusException thrown) {
            Assert.assertNotEquals("", thrown.getMessage());
        }
    }


}
